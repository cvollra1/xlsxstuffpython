from controller.workBookReader import WorkBookReader
class TestErrorRaised():

    def testRaiseExceptionForNoneTypeWorkbook(self):
        try:
            test = WorkBookReader(None)
        except:
            print("Exception Thrown: Value Error" )
    def testRaiseExceptionForFilePathWithoutExcelCompatabileExtension(self):
        try:
            test = WorkBookReader("test.txt")
        except:
            print("Exception Thrown: Value Error")

    def testValidConstuctionOfEditorWithXLSX(self):
        test = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        print(test.getWorkbookObject())

    def testGetColumnOfWorkSheet(self):
        test = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        print(test.getColumnOrRowOfWorkSheet("vDatastore", "A"))

    def testGetCellValue(self):
        test = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        print(test.getValueOfCell("vDatastore", "A2"))

    def testGetColumnOrRowOfWorkSheetRow(self):
        test = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        print(test.getColumnOrRowOfWorkSheet("vDatastore", "1"))
test = TestErrorRaised()

# test.testRaiseExceptionForNoneTypeWorkbook()
#
# test.testRaiseExceptionForFilePathWithoutExcelCompatabileExtension()
#
# test.testValidConstuctionOfEditorWithXLSX()

test.testGetColumnOfWorkSheet()
test.testGetColumnOrRowOfWorkSheetRow()

test.testGetCellValue()