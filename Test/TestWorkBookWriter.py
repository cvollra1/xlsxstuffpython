from controller.WorkBookWriter import WorkBookWriter
from controller.workBookReader import WorkBookReader
class TestWorkBookWriter():

    def testWorkWrite(self):
        test = WorkBookWriter("../excelFiles/test.xlsx","sheet1")
        testRead = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        test.writeToColumn(0,testRead.getColumnOrRowOfWorkSheet("vDatastore", "A"))

    def testWorkWriteRow(self):
        test = WorkBookWriter("../excelFiles/test.xlsx", "sheet1")
        testRead = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        test.writeToRow(0,testRead.getColumnOrRowOfWorkSheet("vDatastore", "A"))
    def testWorkWriteRowThenWriteToCell(self):
        test = WorkBookWriter("../excelFiles/test.xlsx", "sheet2")
        test.writeToCell(3,3,"TESTING")
        testRead = WorkBookReader("../excelFiles/DC01-NTVC01--12082019.xlsx")
        test.writeToRow(0,testRead.getColumnOrRowOfWorkSheet("vDatastore", "A"))
        test.close()

test = TestWorkBookWriter()

# test.testWorkWrite()
# test.testWorkWriteRow()
test.testWorkWriteRowThenWriteToCell()